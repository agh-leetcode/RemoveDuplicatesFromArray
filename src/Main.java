public class Main {

    //to work correctly array should be sorted

    public static void main(String[] args) {
        Main main = new Main();
        main.runTheApp();
    }

    private void runTheApp() {
        int arr[] = new int[]{1,2,2,4,5};
        //int arr[] = new int[]{2,4,4,4,5,89}
        printArrayElements(arr,arr.length);
        int length = removeDuplicates(arr);
        printArrayElements(arr, length);
    }

    private void printArrayElements(int[] arr, int length) {
        System.out.println("Array elements: ");
        for (int i = 0; i < length; i++) {
            System.out.print(arr[i]+ " ");
        }
        System.out.println();
    }

    private int removeDuplicates(int[]arr){
        if (arr==null||arr.length==0){
            return 0;
        }
        System.out.println("\nArray length before: " + arr.length);
        int length=0;
        int currentElement = arr[0];
        for (int i = 1; i < arr.length; i++) {
            if (arr[i]!=currentElement){
                arr[length++]=currentElement;
                currentElement = arr[i];
            }
        }
        arr[length++] = currentElement;
        System.out.println("Array length after: " + length);
        /*for (int i : arr) {
            System.out.print(i);
        }*/
        System.out.println();
        return length;
    }
}
